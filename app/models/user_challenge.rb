class UserChallenge < ApplicationRecord
  belongs_to :user
  belongs_to :challenge

  validates :points_earned, numericality: { greater_than_or_equal_to: 0 }

  # Method to update points
  def add_points(points)
    update(points_earned: points_earned + points)
  end
end