class Leaderboard < ApplicationRecord
  belongs_to :challenge
  belongs_to :user

  validates :rank, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
end