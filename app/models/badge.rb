class Badge < ApplicationRecord
    has_many :user_badges
  
    validates :name, presence: true
  end