class Challenge < ApplicationRecord
    has_many :user_challenges
    has_many :leaderboards
    has_many :feedbacks
  
    validates :name, presence: true
  end
  