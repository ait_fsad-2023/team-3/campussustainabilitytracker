class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :user_challenges
  has_many :leaderboards
  has_many :user_badges
  has_many :notifications
  has_many :feedbacks
  has_one_attached :profile_picture

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates_each :email do |record, attr, value|
    unless value.downcase.end_with?('@ait.ac.th')
      record.errors.add(attr, 'must be an AIT email')
    end
  end

  attribute :admin, :boolean, default: false
end
