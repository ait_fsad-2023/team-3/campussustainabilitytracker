class DashboardsController < ApplicationController
    before_action :authenticate_user!
  
    def index
      @user_challenges = current_user.user_challenges.joins(:challenge).where('challenges.end_date > ?', DateTime.now)
      @leaderboard_positions = Leaderboard.where(user: current_user).includes(:challenge)
      @notifications = current_user.notifications
    end    
end
  