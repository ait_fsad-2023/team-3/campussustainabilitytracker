class ChallengesController < ApplicationController
    before_action :authenticate_user!, only: [:show, :join]
  
    def index
        @challenges = Challenge.where("end_date > ?", DateTime.now)
    end    
  
    def show
      @challenge = Challenge.find(params[:id])
      @challenge.feedbacks = @challenge.feedbacks.includes(:user) # Load feedbacks with users
    end     

    def join
      challenge = Challenge.find(params[:id])
      user_challenge = current_user.user_challenges.create(
        challenge: challenge, 
        points_earned: 0
      )
      
      if user_challenge.persisted?
        redirect_to dashboard_path, notice: 'You have successfully joined the challenge!'
      else
        redirect_to challenge, alert: "There was an issue joining the challenge: #{user_challenge.errors.full_messages.to_sentence}"
      end
    end    
end
