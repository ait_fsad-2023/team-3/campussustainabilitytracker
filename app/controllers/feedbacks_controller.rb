class FeedbacksController < ApplicationController
  before_action :authenticate_user!

  def create
    @challenge = Challenge.find(params[:challenge_id])
    @feedback = @challenge.feedbacks.new(feedback_params)
    @feedback.user = current_user

    if @feedback.save
      redirect_to challenge_path(@challenge), notice: 'Feedback submitted successfully.'
    else
      redirect_to challenge_path(@challenge), alert: 'Unable to submit feedback.'
    end
  end

  private

  def feedback_params
    params.require(:feedback).permit(:message)
  end
end
