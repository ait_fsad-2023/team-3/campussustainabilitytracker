class HomeController < ApplicationController
  def index
    @challenges = Challenge.where("end_date > ?", DateTime.now)
  end
end
