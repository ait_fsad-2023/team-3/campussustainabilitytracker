class LeaderboardsController < ApplicationController
    before_action :authenticate_user!
  
    def index
      @leaderboards = Leaderboard.all
    end

  end
  