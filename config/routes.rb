Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'home/index'
  get 'challenges', to: 'challenges#index'
  get 'challenges/:id', to: 'challenges#show', as: :challenge
  get 'dashboard', to: 'dashboards#index'
  get 'profile', to: 'users#show', as: :profile

  post 'challenges/:id/join', to: 'challenges#join', as: :join_challenge

  resources :challenges do
    resources :feedbacks, only: [:create]
  end

  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  root to: 'home#index'
end
