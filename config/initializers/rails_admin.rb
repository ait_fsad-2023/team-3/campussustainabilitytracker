RailsAdmin.config do |config|
  config.asset_source = :importmap

  # Devise Authentication and Admin Check
  config.authenticate_with do
    warden.authenticate! scope: :user
    unless current_user.admin?
      flash[:alert] = "You are not permitted to view this page."
      redirect_to main_app.root_path
    end
  end
  config.current_user_method(&:current_user)

  # Models Configuration
  ## Challenge
  config.model 'Challenge' do
    list do
      field :name
      field :start_date
      field :end_date
    end
    edit do
      field :name
      field :description
      field :start_date
      field :end_date
    end
  end

  ## Badge
  config.model 'Badge' do
    list do
      field :name
    end
    edit do
      field :name
      field :description
    end
  end

  ## Feedback
  config.model 'Feedback' do
    list do
      field :user
      field :message
    end
    edit do
      field :user
      field :message
    end
  end

  ## Leaderboard
  config.model 'Leaderboard' do
    list do
      field :user
      field :challenge
      field :rank
    end
    edit do
      field :user
      field :challenge
      field :rank
    end
  end

  ## Notification
  config.model 'Notification' do
    list do
      field :user
      field :message
    end
    edit do
      field :user
      field :message
    end
  end

  ## UserBadge
  config.model 'UserBadge' do
    list do
      field :user
      field :badge
    end
    edit do
      field :user
      field :badge
    end
  end

  ## UserChallenge
  config.model 'UserChallenge' do
    list do
      field :user
      field :challenge
      field :points_earned
    end
    edit do
      field :user
      field :challenge
      field :points_earned
    end
  end

  ## User
  config.model 'User' do
    list do
      field :email
      field :admin
    end
    edit do
      field :email
      field :admin
      # Exclude sensitive fields like password, etc.
    end
  end

  # General Actions
  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new                           # enables creating new records
    export
    bulk_delete
    show
    edit                          # enables editing existing records
    delete                        # enables deleting records
    show_in_app
  end
end
