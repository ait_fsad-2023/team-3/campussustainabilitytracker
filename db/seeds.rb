# Clear existing data
User.destroy_all
Challenge.destroy_all
UserChallenge.destroy_all
Leaderboard.destroy_all
Badge.destroy_all
UserBadge.destroy_all
Notification.destroy_all
Feedback.destroy_all

# Users
user1 = User.create(email: 'john@ait.ac.th', password: 'password123', password_confirmation: 'password123', personal_information: 'Info about John', admin: true) # Making John an admin
user2 = User.create(email: 'jane@ait.ac.th', password: 'password123', password_confirmation: 'password123', personal_information: 'Info about Jane')
user3 = User.create(email: 'st123843@ait.ac.th', password: 'Cqrity!234', password_confirmation: 'Cqrity!234', personal_information: 'Info about Nyein')

# Profile pictures (if using Active Storage, and if images are available)
# Example:
# if Rails.env.development?
#   user1.profile_picture.attach(io: File.open(Rails.root.join('path', 'to', 'image1.jpg')), filename: 'image1.jpg')
#   user2.profile_picture.attach(io: File.open(Rails.root.join('path', 'to', 'image2.jpg')), filename: 'image2.jpg')
# end

# Challenges
challenge1 = Challenge.create(name: 'Water Conservation', description: 'Save water in various activities', detailed_description:'This challenge focuses on encouraging participants to adopt water-saving techniques in their daily lives. The goal is to raise awareness about the importance of water conservation and to demonstrate how small changes in behavior can lead to significant water savings. Participants are encouraged to implement various water-saving methods such as fixing leaks, taking shorter showers, using water-efficient appliances, and practicing rainwater harvesting. The challenge will track progress through a daily log where participants record their water-saving activities. Additional resources and tips will be provided to help participants successfully reduce their water usage. By the end of this challenge, participants will have not only contributed to water conservation efforts but also developed sustainable habits that can make a lasting impact.', start_date: DateTime.now, end_date: DateTime.now + 30)
challenge2 = Challenge.create(name: 'Energy Saving', description: 'Reduce energy consumption', detailed_description:'The Energy Saving Challenge aims to reduce energy consumption among participants by promoting energy-efficient practices. Participants will be tasked with implementing strategies to lower their energy use, such as turning off lights and electronics when not in use, utilizing energy-efficient appliances, and optimizing home insulation. The challenge will include educational materials on how energy consumption impacts the environment and practical tips for reducing personal energy use. Participants will log their daily energy-saving actions, and progress will be monitored throughout the challenge. This initiative not only helps in reducing the carbon footprint but also can lead to significant savings on energy bills. The ultimate goal is to foster a community of environmentally conscious individuals who are committed to making energy-saving a part of their everyday lives.', start_date: DateTime.now, end_date: DateTime.now + 30)
challenge3 = Challenge.create(name: 'Waste Reduction', description: 'Minimize waste generation in daily life', detailed_description: 'This challenge encourages participants to reduce the amount of waste they produce. It focuses on promoting practices like recycling, composting, and choosing reusable over disposable items. Participants will learn about the impact of waste on the environment and will be provided with strategies to minimize their waste footprint. This includes tips on reducing packaging waste, composting organic waste, and donating or repurposing items instead of discarding them. Participants will keep a log of their waste reduction efforts, and the challenge will offer weekly goals to gradually increase their waste-reducing activities. This initiative aims to foster a culture of sustainability and environmental responsibility.', start_date: DateTime.now, end_date: DateTime.now + 30)
challenge4 = Challenge.create(name: 'Community Clean-up', description: 'Participate in local community clean-up activities', detailed_description: 'The Community Clean-up Challenge is an initiative to engage participants in improving their local environment. This challenge involves organizing and participating in clean-up drives in local neighborhoods, parks, and beaches. Participants will be guided on how to safely collect and dispose of litter, and will learn about the impact of litter on wildlife and natural ecosystems. The challenge will also emphasize the importance of community involvement in environmental stewardship. Participants will be encouraged to document their clean-up activities and share their experiences to inspire others. The aim is to create cleaner, healthier, and more beautiful communities through collective action.', start_date: DateTime.now, end_date: DateTime.now + 30)
challenge5 = Challenge.create(name: 'Healthy Eating', description: 'Adopt a healthy and sustainable diet', detailed_description: 'This challenge focuses on encouraging participants to adopt a healthier and more environmentally sustainable diet. Participants will be guided through the process of incorporating more plant-based foods into their meals, understanding nutritional labels, and making informed food choices. The challenge will provide recipes, meal planning tips, and information on the environmental impact of various food choices. Participants will also learn about the benefits of locally sourced and organic foods. By keeping a food diary, participants can track their progress and reflect on their dietary changes. The goal is to promote a healthier lifestyle while also considering the environmental footprint of our food choices.', start_date: DateTime.now, end_date: DateTime.now + 30)

# UserChallenges
UserChallenge.create(user: user1, challenge: challenge1, points_earned: 10)
UserChallenge.create(user: user2, challenge: challenge2, points_earned: 15)

# Leaderboards
Leaderboard.create(challenge: challenge1, user: user1, rank: 1)
Leaderboard.create(challenge: challenge2, user: user2, rank: 1)

# Badges
badge1 = Badge.create(name: 'Water Saver', description: 'Awarded for water conservation efforts')
badge2 = Badge.create(name: 'Energy Saver', description: 'Awarded for energy saving actions')

# UserBadges
UserBadge.create(user: user1, badge: badge1)
UserBadge.create(user: user2, badge: badge2)

# Notifications
Notification.create(user: user1, message: 'New challenge available: Energy Saving')
Notification.create(user: user2, message: 'Congratulations! You earned the Water Saver badge')

# Feedback
Feedback.create(user: user1, message: 'Great app for sustainability!')
Feedback.create(user: user2, message: 'Looking forward to more challenges')