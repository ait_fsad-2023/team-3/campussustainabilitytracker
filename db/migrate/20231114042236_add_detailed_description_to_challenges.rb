class AddDetailedDescriptionToChallenges < ActiveRecord::Migration[7.0]
  def change
    add_column :challenges, :detailed_description, :text
  end
end
